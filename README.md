<figure>

![Ilustración en Mary LaFetra Russell, *Mother Goose*, Gabriel Sons & Company, Nueva York, 1917. (Foto: Pinterest / WOODNISSE)](https://gitlab.com/prolibreros/poesia-de-muchas-manos/-/raw/no-masters/portada/portada.small.png)

  <figcaption>

Ilustración en Mary LaFetra Russell, *Mother Goose*. Nueva York: Gabriel Sons & Company, 1917. Foto: Pinterest / WOODNISSE.

  </figcaption>
</figure>

[Descargar PDF](https://gitlab.com/prolibreros/poesia-de-muchas-manos/-/raw/no-masters/bebes.pdf?inline=false)
